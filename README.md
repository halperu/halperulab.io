# Halfront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




letc: number= 1 ;
letb: number =2;
lete: string='1';
letf: string = '2';
console.log(c+b);
console.log(e+f);

letg: boolean= true;
leth: object = {};
console.log(g);
console.log(h);

let i =[c,b,e,f,g,h];
console.log(i);

letj: boolean []=[true,g]
console.log(j);

letk: object[] = [{},h];
console.log(k);

letl: any[]=[1,'string',h];
console.log(l);



questions:
    * Se agregó los siguientes campos "si_niveles,si_nivelcurrent,si_stepcurrent,si_end" a centro_educativo para contemplar el registro de seguimiento en los pasos del director al momento de actualizar setup inicial
    * Se agregó en parámetros de recuperación en el login lo siguiente: "si_niveles,si_nivelcurrent,si_stepcurrent,tipousuario,si_end", "si" es un alias de setup_initial y tipousuario se agregó para obtener tipo de usuario del logeado y redireccionarlo según sea el caso
    * Se agregó related_name en table usuario y centro_educativo de class UsuarioCentroEducativo para llegar más rápido a data
    ----------
    * Se agregó nuevo servicio centroeducativo/centro_educativo_updsi only put para update los campos de setupinitial
    * Se agregó nuevo servicio centroeducativo/centro_educativo_getsi only get para get los campos de setupinitial
    * serializer no tiene name turno y grado en api get seccion
    * serializer no tiene name turno y grado en api get gradocurso
    * si_nivelesupd se agregó a campo de centroeducativo


    * En servicios luego de logearse me pide colegio y centro educativo, ya no debería pedir ya se envía el json web token, en el api back se debería obtener esos datos con el json web token de header.
    * No está filtrando por json web token (obteniendo colegio y centro educativo), y filtrar ejemplo get curso, get leyenda, etc
    * anio en api formula, registrar por back autmático
    * el divisor no se sabe donde se guardará, es autogenerado, por parámetros ingresado en el dividendo?
    * no se entiende tipo_persona , si en usuario centro educativo tiene tipo usuario
    ------------
    * seccion falta filtrar por nivel desde api step4
    * step7 metodo de evaluación, es por centro educativo no por nivel, no tiene sentido q se repita por nivel
pend:
    * datainitial ls debe ser diferenciado por ce y col
    * los servicios devuelven 500 cuando el jwt expira
    * order descendente en api lista curso
    * falta añadir un delete all por jwt en las formulas leyendas


pend for my:
    check step1 y steps 2, prioridad step2
    save y get data paso 3
    load data step3 de json


https://www.npmjs.com/package/angular-middleware