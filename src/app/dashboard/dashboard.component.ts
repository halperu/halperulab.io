import { Component, OnInit } from '@angular/core';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../../assets/css/dashboard.css']
})
export class DashboardComponent implements OnInit {

  constructor(private gl_login: GlobalsLogin) { 
    //localStorage.removeItem('ls_datasteps');
  }

  ngOnInit() {
  }

}