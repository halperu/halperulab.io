import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from '../globals';
import { SetupinitialService } from '../services/setupinitial.service';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step8',
  templateUrl: './step8.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step8Component {
  datasteps: any={};
  si_end: boolean=false;
  nivel_next: any={};
  constructor(private gl_login: GlobalsLogin, private router: Router, private globals: Globals, private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    if( parseInt(this.datasteps.niveles.slice(-1)[0].id) == parseInt(this.datasteps.nivel_upd.id) ){
      this.si_end=true;
    }else{
      let status_next=false;
      this.datasteps.niveles.forEach(element => {
        if(status_next){
          this.nivel_next=element
          status_next=false;
        }else{
          if(parseInt(element.id)==parseInt(this.datasteps.nivel_upd.id)){
            status_next=true;
          }
        }

      });
    }
  }

  onSubmit(){
    if(this.si_end){
      this.router.navigate(['dashboard'])
    }else{
      this.datasteps.nivel_upd=this.nivel_next;
      localStorage.ls_datasteps = JSON.stringify(this.datasteps);
      let nd=this.setupinitialService.centro_educativo_updsi({si_nivelcurrent:this.nivel_next.id,si_stepcurrent:2,}).subscribe((result) => {
        this.router.navigate(['setupinitial/step2'])
      }, (err) => {});
    }
  }  
}
