import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatorSetupinitialComponent } from './navigator-setupinitial.component';

describe('NavigatorSetupinitialComponent', () => {
  let component: NavigatorSetupinitialComponent;
  let fixture: ComponentFixture<NavigatorSetupinitialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigatorSetupinitialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatorSetupinitialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
