import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { Globals } from '../globals';
import { SetupinitialService } from '../services/setupinitial.service';
import { resolve, reject } from 'q';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step4Component {
  //listaSeccion
  dataAdd: any={};
  datasteps: any={};
  grados: any=[];
  new_secciones:any=[];
  secciones: any=[];
  constructor(private gl_login: GlobalsLogin, private router: Router, private globals: Globals, private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    globals.ls_grados.forEach(element => {
      if(element.nivel==this.datasteps.nivel_upd.id){
        this.grados.push(element);
      }
    });

    this.setupinitialService.getSecciones().subscribe((result) => {
      this.secciones=result;
    }, (err) => {});    


    /*
    var p1 = new Promise((resolve, reject) => { 
      setTimeout(resolve, 1000, "one"); 
    }); 
    var p2 = new Promise((resolve, reject) => { 
      setTimeout(resolve, 2000, "two"); 
    });
    var p3 = new Promise((resolve, reject) => {
      setTimeout(resolve, 3000, "three" );
    });
    var p4 = new Promise((resolve, reject) => {
      setTimeout(resolve, 9000, "four" );
    });
    var p5 = new Promise((resolve, reject) => {
      setTimeout(resolve, 10000, "five" );
    });
    
    Promise.all([p1, p2, p3, p4, p5]).then(values => { 
      console.log('rogged values:'+values);
    }, reason => {
      console.log('Error>>'+reason)
    });*/
    





  }
  rowAdd(){
    let xgrado=this.dataAdd.grado.split('-');
    let xturno=this.dataAdd.turno.split('-');
    let data_seccion={
      colegio:localStorage.userCOL,
      nivel:this.datasteps.nivel_upd.id,
      ciclos_academicos:this.datasteps.data_nivel.ciclos,
      centro_educativo:localStorage.userCE,
      horas_academicas:this.datasteps.data_nivel.horas_academicas,
      nombre:this.dataAdd.seccion,
      turno:xturno[0],
      grado:xgrado[0]
    }
    let nd=this.setupinitialService.addSecciones(data_seccion).subscribe((result) => {
      this.secciones.push(result);
    }, (err) => {});
  }
  rowRemove(id,key){
    this.secciones.splice(key, 1);
    let nd=this.setupinitialService.deleteSeccion(id).subscribe((result) => {
      
    }, (err) => {});
  }

  onSubmit(){
    let nd=this.setupinitialService.centro_educativo_updsi({si_stepcurrent:5}).subscribe((result) => {
      this.datasteps.step=5;
      localStorage.ls_datasteps = JSON.stringify(this.datasteps);
      this.router.navigate(['setupinitial/step5'])
    }, (err) => {});
  }
}