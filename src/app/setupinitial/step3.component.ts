import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { Globals } from '../globals';
import { SetupinitialService } from '../services/setupinitial.service';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step3Component {
  form: any={turnos:[],horas_academicas:1,ciclos:1};
  datasteps: any={};
  constructor(private gl_login: GlobalsLogin, private router: Router, globals: Globals, private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    this.form.turnos=globals.ls_turnos;
  }
  onSubmit(){
    let turnos_checked = [];
    this.form.turnos.forEach(element => {
      if(element.checked){
        turnos_checked.push({'id':element.id,'nombre':element.nombre})
      }
    });
    let xsi_datanivel=[]
    xsi_datanivel.push({nivel:this.datasteps.nivel_upd.id,data:{turnos_checked:turnos_checked,horas_academicas:this.form.horas_academicas,ciclos:this.form.ciclos}})
    let si_datanivel=JSON.stringify(xsi_datanivel)
    let stepcurrent=4;
    let nd=this.setupinitialService.centro_educativo_updsi({si_stepcurrent:stepcurrent,si_datanivel:si_datanivel}).subscribe((result) => {
      this.datasteps.data_nivel.turnos=turnos_checked;
      this.datasteps.data_nivel.horas_academicas=this.form.horas_academicas;
      this.datasteps.data_nivel.ciclos=this.form.ciclos;
      this.datasteps.step=stepcurrent;
      localStorage.ls_datasteps = JSON.stringify(this.datasteps);
      this.router.navigate(['setupinitial/step4'])
    }, (err) => {});
  }
}
