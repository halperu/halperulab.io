import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { Globals } from '../globals';
import { SetupinitialService } from '../services/setupinitial.service';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step7',
  templateUrl: './step7.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step7Component {
  dataAdd: any={colegio:localStorage.userCOL,centro_educativo:localStorage.userCE,};
  datasteps: any={};
  new_formula:any=[];
  dataForm:any=[];
  leyendas:any=[];
  constructor(private gl_login: GlobalsLogin, private router: Router, private globals: Globals, private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    this.setupinitialService.getLeyendas().subscribe((result) => {
      this.leyendas=result
    }, (err) => {});
  }
  rowAdd(){
    let nd=this.setupinitialService.addLeyendas(this.dataAdd).subscribe((result) => {
      this.leyendas.push(result);
    }, (err) => {});
  }
  rowRemove(id,key){
    this.leyendas.splice(key, 1);
    let nd=this.setupinitialService.deleteLeyenda(id).subscribe((result) => {

    }, (err) => {});
  }
  addFormula(xformula, data, callback){
    // falta aquí añadir remove all formulaleyenda como una promesa más
    let nd=this.setupinitialService.addFormulas(data).subscribe((result) => {
      let xleyformula=xformula.split('+');
      xleyformula.forEach(element => {
        console.log(element)
        let xleyform=element.split('*');
        let sleyenda=null;
        this.leyendas.forEach(xitem => {
          if(xitem.abreviatura == xleyform[1].trim()){ sleyenda=xitem }
        });
        let xnewform=this.addFormulaLeyenda(
          {peso:xleyform[0],formula:result.id,leyenda:sleyenda.id}
          ,function(){}
        )
        this.new_formula.push(xnewform);        
      });

      return callback(result)
    }, (err) => {});
    return nd;
  }  
  addFormulaLeyenda(data, callback){
    const nd=this.setupinitialService.addFormulasLeyendas(data).subscribe((result) => {
      return callback(result)
    }, (err) => {});
    return nd;
  }
  onSubmit(){
    let xnewform=this.addFormula(this.dataForm.formula,{anio_academico:2019,colegio:localStorage.userCOL,centro_educativo:localStorage.userCE},function(){})
    this.new_formula.push(xnewform);
    Promise.all(this.new_formula).then(values => {
      let si_end=false
      if( parseInt(this.datasteps.niveles.slice(-1)[0].id) == parseInt(this.datasteps.nivel_upd.id) ) si_end=true;
      let stepcurrent=8;
      let nd=this.setupinitialService.centro_educativo_updsi({si_stepcurrent:stepcurrent,si_end:si_end,si_nivelesupd:this.datasteps.nivel_upd.id}).subscribe((result) => {
        this.datasteps.step=stepcurrent;
        localStorage.ls_datasteps = JSON.stringify(this.datasteps);
        this.router.navigate(['setupinitial/step8'])
      }, (err) => {});      
      
    }, reason => {});
  }
}
