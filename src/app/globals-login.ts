import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class GlobalsLogin {
    ls_niveles: any=[{'id':1,'checked':false,'nombre':'inicial'},{'id':2,'checked':false,'nombre':'primaria'},{'id':3,'checked':false,'nombre':'secundaria'}];
    ls_turnos: any=[{'id':1,'checked':false,'nombre':'mañana'},{'id':2,'checked':false,'nombre':'tarde'},{'id':3,'checked':false,'nombre':'noche'}];
    ls_grados: any=[
        {
            "id": 1,
            "nombre": "1",
            "nivel": 2
        },
        {
            "id": 2,
            "nombre": "2",
            "nivel": 2
        },
        {
            "id": 3,
            "nombre": "3",
            "nivel": 2
        },
        {
            "id": 4,
            "nombre": "4",
            "nivel": 2
        },
        {
            "id": 5,
            "nombre": "5",
            "nivel": 2
        },
        {
            "id": 6,
            "nombre": "6",
            "nivel": 2
        },
        {
            "id": 7,
            "nombre": "1",
            "nivel": 3
        },
        {
            "id": 8,
            "nombre": "2",
            "nivel": 3
        },
        {
            "id": 9,
            "nombre": "3",
            "nivel": 3
        },
        {
            "id": 10,
            "nombre": "4",
            "nivel": 3
        },
        {
            "id": 11,
            "nombre": "5",
            "nivel": 3
        },
        {
            "id": 12,
            "nombre": "1",
            "nivel": 1
        },
        {
            "id": 13,
            "nombre": "2",
            "nivel": 1
        },
        {
            "id": 14,
            "nombre": "3",
            "nivel": 1
        },
        {
            "id": 15,
            "nombre": "4",
            "nivel": 1
        }
    ]
    
    url_setupinitial: any=[
        '/setupinitial/step1',
        '/setupinitial/step2',
        '/setupinitial/step3',
        '/setupinitial/step4',
        '/setupinitial/step5',
        '/setupinitial/step6',
        '/setupinitial/step7',
        '/setupinitial/step8'
    ]
    constructor(private router: Router){
        if(!localStorage.userToken){
            this.router.navigate(['/'])
        }else{
            if(localStorage.ls_datasteps){
                if( !this.url_setupinitial.includes(this.router.url) ){
                    this.router.navigate(['/setupinitial/step'+localStorage.ls_stepcurrent]);
                }
            }else{
                if( this.url_setupinitial.includes(this.router.url) ){
                    this.router.navigate(['/dashboard']);
                }
            }
        }
        
    }

}