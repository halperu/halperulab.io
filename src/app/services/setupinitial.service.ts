import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpBackend } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class SetupinitialService {

  constructor(private globals: Globals, private http: HttpClient) { }
  centro_educativo_getsi(): Observable<any> {
    return this.http.get<any>(this.globals.endpoint + 'centroeducativo/centro_educativo_getsi/').pipe(
      tap( (data) => console.log('centro_educativo_getsi') ),
      catchError(this.handleError<any>('centro_educativo_getsi'))
    );
  }
  centro_educativo_updsi(data): Observable<any> {
    return this.http.put<any>(this.globals.endpoint + 'centroeducativo/centro_educativo_updsi/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('centro_educativo_updsi') ),
      catchError(this.handleError<any>('centro_educativo_updsi'))
    );
  }
  getSecciones(): Observable<any> {
    return this.http.get<any>(this.globals.endpoint + 'seccion/').pipe(
      tap( (data) => console.log('getSecciones') ),
      catchError(this.handleError<any>('getSecciones'))
    );
  }  
  addSecciones(data): Observable<any> {
    return this.http.post<any>(this.globals.endpoint + 'seccion/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('addSecciones') ),
      catchError(this.handleError<any>('addSecciones'))
    );
  }
  deleteSeccion(id): Observable<any> {
    return this.http.delete<any>(this.globals.endpoint + 'seccion/'+id ).pipe(
      tap( (data) => console.log('deleteSeccion') ),
      catchError(this.handleError<any>('deleteSeccion'))
    );
  }  
  
  getCursos(): Observable<any> {
    return this.http.get<any>(this.globals.endpoint + 'curso/').pipe(
      tap( (data) => console.log('getCursos') ),
      catchError(this.handleError<any>('getCursos'))
    );
  }
  addCursos(data): Observable<any> {
    return this.http.post<any>(this.globals.endpoint + 'curso/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('addCursos') ),
      catchError(this.handleError<any>('addCursos'))
    );
  }  
  deleteCurso(id): Observable<any> {
    return this.http.delete<any>(this.globals.endpoint + 'curso/'+id ).pipe(
      tap( (data) => console.log('deleteCurso') ),
      catchError(this.handleError<any>('deleteCurso'))
    );
  }    
  
  getLeyendas(): Observable<any> {
    return this.http.get<any>(this.globals.endpoint + 'leyenda/').pipe(
      tap( (data) => console.log('getLeyendas') ),
      catchError(this.handleError<any>('getLeyendas'))
    );
  }
  addLeyendas(data): Observable<any> {
    return this.http.post<any>(this.globals.endpoint + 'leyenda/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('addLeyendas') ),
      catchError(this.handleError<any>('addLeyendas'))
    );
  }
  deleteLeyenda(id): Observable<any> {
    return this.http.delete<any>(this.globals.endpoint + 'leyenda/'+id ).pipe(
      tap( (data) => console.log('deleteLeyenda') ),
      catchError(this.handleError<any>('deleteLeyenda'))
    );
  }    
  addFormulas(data): Observable<any> {
    return this.http.post<any>(this.globals.endpoint + 'formula/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('addFormulas') ),
      catchError(this.handleError<any>('addFormulas'))
    );
  }      
  addFormulasLeyendas(data): Observable<any> {
    return this.http.post<any>(this.globals.endpoint + 'formulaleyenda/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('addFormulasLeyendas') ),
      catchError(this.handleError<any>('addFormulasLeyendas'))
    );
  }
  
  getGradosCursos(): Observable<any> {
    return this.http.get<any>(this.globals.endpoint + 'gradocurso/').pipe(
      tap( (data) => console.log('getGradosCursos') ),
      catchError(this.handleError<any>('getGradosCursos'))
    );
  }
  addGradosCursos(data): Observable<any> {
    return this.http.post<any>(this.globals.endpoint + 'gradocurso/', JSON.stringify(data) ).pipe(
      tap( (data) => console.log('addGradosCursos') ),
      catchError(this.handleError<any>('addGradosCursos'))
    );
  }
  
  deleteGradoCurso(id): Observable<any> {
    return this.http.delete<any>(this.globals.endpoint + 'gradocurso/'+id ).pipe(
      tap( (data) => console.log('deleteGradoCurso') ),
      catchError(this.handleError<any>('deleteGradoCurso'))
    );
  }      
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
