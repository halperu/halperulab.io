import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class InitialService {

  constructor(private globals: Globals, private http: HttpClient) {    
  }
  ngOnInit() {}
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  proccessReturn(url){
    return this.http.get(this.globals.endpoint + url).pipe(
      map(this.extractData));
  }
  getNiveles(): Observable<any> {
    return this.proccessReturn('nivel')
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
