import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatorNologinComponent } from './navigator-nologin.component';

describe('NavigatorNologinComponent', () => {
  let component: NavigatorNologinComponent;
  let fixture: ComponentFixture<NavigatorNologinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigatorNologinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatorNologinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
